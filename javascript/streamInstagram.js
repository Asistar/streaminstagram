var Squery = function( el )
{
	if( typeof el === "function"){
		document.addEventListener("DOMContentLoaded", el);
	}else{
		this.query = el;
		this.self = document.querySelectorAll(el);

	}
	return this;
};

Squery.prototype.forEach = function( func )
{
	for(var i = 0; i < this.self.length; i++)
		func(this.self[i],i);
	return this;
};

Squery.prototype.addClass = function( class_string )
{
	this.forEach(function(self, i){
		if( (self.classList !== null) && (typeof self.classList !== "undefined") )
			self.classList.add(class_string);
		else if(self.className.indexOf(class_string) == -1)
			self.className += " " + class_string;
	});
	return this;
};

Squery.prototype.removeClass = function( class_string )
{
	this.forEach(function(self, i){
		if( (self.classList !== null) && (typeof self.classList !== "undefined") )
			self.classList.remove(class_string);
		else if(self.className.indexOf(class_string) != -1)
			self.split( class_string ).join( "" );
	});
	return this;
};

Squery.prototype.css = function( css_object )
{
	this.forEach(function(self, i){
		for (var key in css_object) {
			self.style.setProperty(key,css_object[key]);
		}
	});
	return this;
};

Squery.prototype.html = function( html )
{
	this.forEach(function(self, i){
		self.innerHTML = html;
	});
	return this;
};

Squery.prototype.append = function( html )
{
	this.forEach(function(self, i){
		self.innerHTML = self.innerHTML + html;
	});
	return this;
};

Squery.prototype.prepend = function( html )
{
	this.forEach(function(self, i){
		self.innerHTML = html + self.innerHTML;
	});
	return this;
};

//TODO: gestire meglio gli eventi
Squery.prototype.on = function( type, callBack )
{
	var questo = this;
	this.forEach(function(self, i){
		if (!self.myListeners) {
			self.myListeners = [];
		}
		if (!self.myListeners[type]) {
			self.myListeners[type] = [];
		}


		self.myListeners[type][questo.query] = callBack;
		self.addEventListener(type, callBack);
	});
	return this;
};

//TODO: gestire meglio gli eventi
Squery.prototype.off = function( type )
{
	var questo = this;
	this.forEach(function(self, i){
		if(self.myListeners && self.myListeners[type] && self.myListeners[type][questo.query]){
			self.removeEventListener(type, self.myListeners[type][questo.query]);
			delete self.myListeners[type][questo.query];
		}
	});
	return this;
};

var S = function(el){ return new Squery(el); };