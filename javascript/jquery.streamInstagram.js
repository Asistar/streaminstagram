(function($) {
	var json2html = {
		//Copyright (c) 2013 Crystalline Technologies
		//
		//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'),
		//  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
		//  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
		//
		//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
		//
		//  THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
		//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
		/* ---------------------------------------- Public Methods ------------------------------------------------ */
		'transform': function(json, transform, options) {

			//create the default output
			var out = {'events':[],'html':''};

			//default options (by default we don't allow events)
			var options = {
				'events':false
			};

			//extend the options
			options = json2html._extend(options);

			//Make sure we have a transform & json object
			if( transform !== undefined || json !== undefined ) {

				//Normalize strings to JSON objects if necessary
				var obj = typeof json === 'string' ? JSON.parse(json) : json;

				//Transform the object (using the options)
				out = json2html._transform(obj, transform, options);
			}

			//determine if we need the events
			// otherwise return just the html string
			if(options.events) return(out);
				else return( out.html );
		},

		/* ---------------------------------------- Private Methods ------------------------------------------------ */

		//Extend options
		'_extend':function(obj1,obj2){
			var obj3 = {};
			for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
			for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
			return obj3;
		},

		//Append results
		'_append':function(obj1,obj2) {
			var out = {'html':'','event':[]};
			if(typeof obj1 !== 'undefined' && typeof obj2 !== 'undefined') {
				out.html = obj1.html + obj2.html;

				out.events = obj1.events.concat(obj2.events);
			}

			return(out);
		},

		//isArray (fix for IE prior to 9)
		'_isArray':function(obj) {
			return Object.prototype.toString.call(obj) === '[object Array]';
		},

		//Transform object
		'_transform':function(json, transform, options) {

			var elements = {'events':[],'html':''};

			//Determine the type of this object
			if(json2html._isArray(json)) {

				//Itterrate through the array and add it to the elements array
				var len=json.length;
				for(var j=0;j<len;++j) {
					//Apply the transform to this object and append it to the results
					elements = json2html._append(elements,json2html._apply(json[j], transform, j, options));
				}

			} else if(typeof json === 'object') {

				//Apply the transform to this object and append it to the results
				elements = json2html._append(elements,json2html._apply(json, transform, undefined, options));
			}

			//Return the resulting elements
			return(elements);
		},

		//Apply the transform at the second level
		'_apply':function(obj, transform, index, options) {

			var element = {'events':[],'html':''};

			//Itterate through the transform and create html as needed
			if(json2html._isArray(transform)) {

				var t_len = transform.length;
				for(var t=0; t < t_len; ++t) {
					//transform the object and append it to the output
					element = json2html._append(element,json2html._apply(obj, transform[t], index, options));
				}

			} else if(typeof transform === 'object') {

				//Get the tag element of this transform
				if( transform.tag === undefined )
					transform.tag = "div";

				//Create a new element
				element.html += '<' + transform.tag;

				//Create a new object for the children
				var children = {'events':[],'html':''};

				//innerHTML
				var html;

				//Look into the properties of this transform
				for (var key in transform) {

					switch(key) {
						case 'tag':
							//Do nothing as we have already created the element from the tag
						break;

						case 'children':
							//Add the children
							if(json2html._isArray(transform.children)) {

								//Apply the transform to the children
								children = json2html._append(children,json2html._apply(obj, transform.children, index, options));
							} else if(typeof transform.children === 'function') {

								//Get the result from the function
								var temp = transform.children.call(obj, obj, index);

								//Make sure we have an object result with the props
								// html (string), events (array)
								// OR a string (then just append it to the children
								if(typeof temp === 'object') {
									//make sure this object is a valid json2html response object
									if(temp.html !== undefined && temp.events !== undefined) children = json2html._append(children,temp);
								} else if(typeof temp === 'string') {

									//append the result directly to the html of the children
									children.html += temp;
								}
							}
						break;

						case 'html':
							//Create the html attribute for this element
							html = json2html._getValue(obj,transform,'html',index);
						break;

						default:
							//Add the property as a attribute if it's not a key one
							var isEvent = false;

							//Check if the first two characters are 'on' then this is an event
							if( key.length > 2 )
								if(key.substring(0,2).toLowerCase() == 'on') {

									//Determine if we should add events
									if(options.events) {

										//if so then setup the event data
										var data = {
											'action':transform[key],
											'obj':obj,
											'data':options.eventData,
											'index':index
										};

										//create a new id for this event
										var id = json2html._guid();

										//append the new event to this elements events
										element.events[element.events.length] = {'id':id,'type':key.substring(2),'data':data};

										//Insert temporary event property (json2html-event-id) into the element
										element.html += " json2html-event-id-"+key.substring(2)+"='" + id + "'";
									}
									//this is an event
									isEvent = true;
								}

							//If this wasn't an event AND we actually have a value then add it as a property
							if( !isEvent){
								//Get the value
								var val = json2html._getValue(obj, transform, key, index);

								//Make sure we have a value
								if(val !== undefined) {
									var out;

									//Determine the output type of this value (wrap with quotes)
									if(typeof val === 'string') out = '"' + val.replace(/"/g, '&quot;') + '"';
									else out = val;

									//creat the name value pair
									element.html += ' ' + key + '=' + out;
								}
							}
						break;
					}
				}

				//close the opening tag
				element.html += '>';

				//add the innerHTML (if we have any)
				if(html) element.html += html;

				//add the children (if we have any)
				element = json2html._append(element,children);

				//add the closing tag
				element.html += '</' + transform.tag + '>';
			}

			//Return the output object
			return(element);
		},

		//Get a new GUID (used by events)
		'_guid':function() {
			var S4 = function() {
			   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
			};
			return (S4()+S4()+"-"+S4()+S4()+"-"+S4()+S4());
		},

		//Get the html value of the object
		'_getValue':function(obj, transform, key,index) {

			var out = '';

			var val = transform[key];
			var type = typeof val;

			if (type === 'function') {
				return(val.call(obj,obj,index));
			} else if (type === 'string') {
				var _tokenizer = new json2html._tokenizer([
					/\$\{([^\}\{]+)\}/
				],function( src, real, re ){
					return real ? src.replace(re,function(all,name){

						//Split the string into it's seperate components
						var components = name.split('.');

						//Set the object we use to query for this name to be the original object
						var useObj = obj;

						//Output value
						var outVal = '';

						//Parse the object components
						var c_len = components.length;
						for (var i=0;i<c_len;++i) {

							if( components[i].length > 0 ) {

								var newObj = useObj[components[i]];
								useObj = newObj;
								if(useObj === null || useObj === undefined) break;
							}
						}

						//As long as we have an object to use then set the out
						if(useObj !== null && useObj !== undefined) outVal = useObj;

						return(outVal);
					}) : src;
				});

				out = _tokenizer.parse(val).join('');
			} else {
				out = val;
			}

			return(out);
		},

		//Tokenizer
		'_tokenizer':function( tokenizers, doBuild ){

			if( !(this instanceof json2html._tokenizer ) )
				return new json2html._tokenizer( tokenizers, doBuild );

			this.tokenizers = tokenizers.splice ? tokenizers : [tokenizers];
			if( doBuild )
				this.doBuild = doBuild;

			this.parse = function( src ){
				this.src = src;
				this.ended = false;
				this.tokens = [ ];
				do {
					this.next();
				} while( !this.ended );
				return this.tokens;
			};

			this.build = function( src, real ){
				if( src )
					this.tokens.push(
						!this.doBuild ? src :
						this.doBuild(src,real,this.tkn)
					);
			};

			this.next = function(){
				var self = this,
					plain;

				self.findMin();
				plain = self.src.slice(0, self.min);

				self.build( plain, false );

				self.src = self.src.slice(self.min).replace(self.tkn,function( all ){
					self.build(all, true);
					return '';
				});

				if( !self.src )
					self.ended = true;
			};

			this.findMin = function(){
				var self = this, i=0, tkn, idx;
				self.min = -1;
				self.tkn = '';

				while(( tkn = self.tokenizers[i++]) !== undefined ){
					idx = self.src[tkn.test?'search':'indexOf'](tkn);
					if( idx != -1 && (self.min == -1 || idx < self.min )){
						self.tkn = tkn;
						self.min = idx;
					}
				}
				if( self.min == -1 )
					self.min = self.src.length;
			};
		}
	};
	var self = "", items = [], currentIndex = 0, instaLoadLock = 0, url = null, nextMaxId = null, startType = null, streamOverlay = null;
	var comments = {
		"tag"      : "li",
		"children" : [
			{ "tag" : "img", "src"   : "${from.profile_picture}"},
			{ "tag" : "div", "class" : "commentItem",
				"children": [
					{ "tag" : "h5",   "html": "${from.full_name}"},
					{ "tag" : "span", "html": "${text}"}
				]
			},
			{ "tag": "div", "class":"clearBoth" }
		]
	};
	var item = {
		"tag"  : "a",
		"href" : "${images.standard_resolution.url}",
		"data-fancybox-group": "instagramStreamItem",
		"children" : [
			{ "tag": "img", "src": "${images.standard_resolution.url}"},
			{ "tag": "div", "class": "user",
				"children" : [
					{ "tag": "img", "src" : "${user.profile_picture}",
						"html": function(obj){
									return obj.user.full_name.split("@").join("");
						}
					}
				]
			},
			{ "tag": "div", "class": "comments",
				"children" : [
					{ "tag": "div", "class" : "user",
						"children" : [
							{ "tag": "div", "class" : "imgContainer",
								"children" : [
									{ "tag": "img", "src" : "${user.profile_picture}"},
									{ "tag": "div", "class" : "stats", "html" : "${likes.count}"}
								]
							},
							{ "tag": "div", "class" : "text",
								"children" : [
									{ "tag": "div", "class" : "name", "html" : "${user.full_name}"},
									{ "tag": "div", "class" : "description", "html" : "${caption.text}"}
								]
							},
							{ "tag"   : "div", "class" : "separator" }
						]
					},
					{ "tag": "ul", "class": "comments",
						"html" : function (obj) {
							if(obj.comments.data.length > 0)
								return json2html.transform(obj.comments.data,comments);
							else
								return "<li class='nocomments'>Nessun commento presente.</li>";
						}
					}
				]
			}
		]
	};
	var overlayItem = function(class_item){
		var classBase = "instagramStreamDialog";
		if(typeof class_item !== "undefined")
			classBase += ((class_item !== null) && (class_item !== "") && (typeof class_item === "string")) ? (" " + class_item) : "";

		return {
		   "tag"      : "div",
		   "class"    : classBase,
		   "children" : [
			   { "tag" : "div", "class" : "image",
					"children" : [
						{ "tag" : "img", "src" : "${images.standard_resolution.url}"},
						{ "tag" : "a", "class" : "prev", "html" : "<span></span>"},
						{ "tag" : "a", "class" : "next", "html" : "<span></span>"},
						{ "tag" : "a", "class" : "close", "html" : ""}
					]
			   },
			   { "tag" : "div", "class" : "document",
				   "children": [
					   { "tag" : "div", "class": "user",
							"children" : [
								{ "tag" : "div", "class": "imgContainer",
									"children" : [
										{ "tag" : "img", "src": "${user.profile_picture}"},
										{ "tag" : "div", "class": "stats", "html": "${likes.count}"}
									]
								},
								{ "tag" : "div", "class": "text",
									"children" : [
										{ "tag" : "div", "class": "name", "html" : "${user.full_name}"},
										{ "tag" : "div", "class": "description", "html": "${caption.text}"}
									]
								},
								{ "tag" : "div", "class" : "separator"}
							]
					   },
					   { "tag" : "ul", "class": "comments",
							"html" : function (obj) {
							   if(obj.comments.data.length > 0)
								   return json2html.transform(obj.comments.data,comments);
							   else
								   return "<li class='nocomments'>Nessun commento presente.</li>";
							}
					   }
				   ]
			   }
		   ]
		};
	};
    var responsiveness = function(questo){
        var resp = function(questo){
            if((questo.width() >= 350) && (questo.width() < 500)){
                questo.removeClass("w350").removeClass("w500").removeClass("w700").removeClass("w1000").removeClass("w1400")
                      .addClass("instagramStream").addClass("w350");
            }else if((questo.width() >= 500) && (questo.width() < 700)){
                questo.removeClass("w350").removeClass("w500").removeClass("w700").removeClass("w1000").removeClass("w1400")
                      .addClass("instagramStream").addClass("w500");
            }else if((questo.width() >= 700) && (questo.width() < 1000)){
                questo.removeClass("w350").removeClass("w500").removeClass("w700").removeClass("w1000").removeClass("w1400")
                      .addClass("instagramStream").addClass("w700");
            }else if((questo.width() >= 1000) && (questo.width() < 1400)){
                questo.removeClass("w350").removeClass("w500").removeClass("w700").removeClass("w1000").removeClass("w1400")
                      .addClass("instagramStream").addClass("w1000");
            }else if(questo.width() >= 1400){
                questo.removeClass("w350").removeClass("w500").removeClass("w700").removeClass("w1000").removeClass("w1400")
                      .addClass("instagramStream").addClass("w1400");
            }
        };
        resp(questo);
        $(window).resize(function(){
            resp(questo);
        });
    };

    var initOverlay = function(){
        streamOverlay = $('<div></div>').addClass("instagramStreamOverlay").appendTo("body");
        eventOverlay();
    };
    var resizeOverlay = function(setOnResize){
        var canResize = true;
        if((typeof setOnResize !== "undefined") && (setOnResize === false))
            canResize = false;

        var resize = function(){
            var streamDialog  = $('.instagramStreamDialog', streamOverlay),
                winHeight     = $(window).height(),
                winWidth      = $(window).width(),
                dialogHeight  = streamDialog.outerHeight();

            if((winHeight / winWidth) < 0.53){
                streamDialog.css('height', "80%");
                streamDialog.css('width', (dialogHeight * 2) + "px");
            }else{
                streamDialog.css('width', "80%");
                var dialogWidth  = streamDialog.outerWidth();
                dialogHeight = (dialogWidth / 2);
                streamDialog.css('height', dialogHeight + "px");
            }

            var marginTop = (winHeight - dialogHeight) / 2;
            streamDialog.css("margin-top", marginTop + "px");
        };

        resize();
        setTimeout(function(){resize();},300);
        if(canResize){
            $(window).resize(function(){
                resize();
                setTimeout(function(){resize();},30);
            });
        }
    };
    var eventOverlay = function(){
        $(streamOverlay).on('click',function(){
            streamOverlay.removeClass('opened');
            setTimeout(function(){streamOverlay.removeClass('opening');},200);
        });

        $(streamOverlay).on('click', "*",function(e){
            e.stopPropagation();
        });

        $(streamOverlay).on('click', ".instagramStreamDialog .close", function(){
            streamOverlay.removeClass('opened');
            setTimeout(function(){streamOverlay.removeClass('opening');},200);
        });

        $(streamOverlay).on('click', ".instagramStreamDialog .prev, .instagramStreamDialog .prev > span", function(e){
            e.preventDefault();
            prevItem();
        });

        $(streamOverlay).on('click', ".instagramStreamDialog .next, .instagramStreamDialog .next > span", function(e){
            e.preventDefault();
            nextItem();
        });

        $(document).keydown(function(e){
            if($(streamOverlay).hasClass("opened")){
                if(e.keyCode == 37) {
                    prevItem();
                    return false;
                }else if(e.keyCode == 39) {
                    nextItem();
                    return false;
                }else if(e.keyCode == 27) {
                    streamOverlay.removeClass('opened');
                    setTimeout(function(){streamOverlay.removeClass('opening');},200);
                }
            }
        });
    };

    var nextItem = function(){
        if(currentIndex >= items.length -6)
            self.streamInstagram( "update" );

        if( currentIndex < items.length - 1 ){
            currentIndex += 1;
            var currentItem = [ items[currentIndex] ];
            streamOverlay.append(json2html.transform(currentItem,overlayItem("hideRight")));
            $(".instagramStreamDialog",streamOverlay).first().addClass('hideLeft');
            resizeOverlay(false);
            setTimeout(function(){
                $(".instagramStreamDialog",streamOverlay).first().remove();
                $(".instagramStreamDialog",streamOverlay).removeClass('hideRight');
            },300);
        }else{
            console.log("sei alla fine dello stream");
        }
    };
    var prevItem = function(){
        if( currentIndex > 0 ){
            currentIndex -= 1;
            var currentItem = [ items[currentIndex] ];
            streamOverlay.append(json2html.transform(currentItem,overlayItem("hideLeft")));
            $(".instagramStreamDialog",streamOverlay).first().addClass('hideRight');
            resizeOverlay(false);
            setTimeout(function(){
                $(".instagramStreamDialog",streamOverlay).first().remove();
                $(".instagramStreamDialog",streamOverlay).removeClass('hideLeft');
            },300);
        }else{
            console.log("sei all'inizio dello stream");
        }
    };

    var methods = {
        init : function( settings )
        {
            if(instaLoadLock === 0){
                instaLoadLock = 1;
                var urlPart1 = "https://api.instagram.com/v1/", urlPart2 = "/media/recent?client_id=";
                var options = {
                    "clientID": null,
                    "by": "users", //"by": "tag"
                    "user_id"  : null,
                    "hashtag"  : null,
                    "maxTag"   : null,
                    "onSuccess": null,
                    "onError"  : null,
                    "onClick"  : null,
                    "onScroll" : null
                };
                $.extend(options, settings);

                if((options.by !== "users") && (options.by !== "tags")){
                    console.log("Error!");
                    console.log('example1: \$("[expression]").streamInstagram( "init", { clientID: "87af82506cb14wer2345fgf", by: "users", "user_id": 375346929 } );');
                    console.log('example2: \$("[expression]").streamInstagram( "init", { clientID: "87af82506cb14wer2345fgf", by: "tags", "hashtag": "trentinodavivere" } );');
                    return false;
                }

                if(options.by === "users"){
                    if( (typeof options.user_id === "undefined") || (options.user_id === null) ){
                        console.log("Error for user stream!");
                        console.log('usage: \$("[expression]").streamInstagram( "init", { clientID: "87af82506cb14wer2345fgf", by: "users", "user_id": 375346929 } );');
                        return false;
                    }
                    url = urlPart1 + options.by + "/" + options.user_id + urlPart2 + options.clientID;
                    startType = "max_id";
                }

                if(options.by === "tags"){
                    if( (typeof options.hashtag === "undefined") || (options.hashtag === null) ){
                        console.log("Error for tag stream!");
                        console.log('usage: \$("[expression]").streamInstagram( "init", { clientID: "87af82506cb14wer2345fgf", by: "tags", "hashtag": "trentinodavivere" } );');
                        return false;
                    }
                    startType = "max_tag_id";
                    url = urlPart1 + options.by + "/" + options.hashtag + urlPart2 + options.clientID;
                }

                $.ajax({
                    url: url,
                    dataType: "jsonp",
                    success: function (data) {
						console.log(data.data);
                        self.addClass("instagramStream").html(json2html.transform(data.data,item));
                        responsiveness(self);
                        nextMaxId = data.pagination.next_max_id;
                        items = items.concat(data.data);

                        $('a > img',self).load(function(){
                            $(this).parent().addClass("visible");
                        });

                        if(typeof options.onSuccess === 'function'){
                            options.onSuccess(self);
                        }

                        if(typeof options.onScroll === 'function'){
                            self.scroll(function(){
                                 options.onScroll(self);
                            });
                        }

                        initOverlay();

                        self.on('click',"a",function(e){
                            e.preventDefault();

                            if(typeof options.onClick === 'function'){
                                options.onClick($(this));
                            }

                            currentIndex = $("a",self).index($(this));
                            var currentItem = [ items[currentIndex] ];

                            streamOverlay.html(json2html.transform(currentItem,overlayItem()));
                            streamOverlay.addClass('opening');
                            setTimeout(function(){streamOverlay.addClass('opened');},300);
                            resizeOverlay();

                        });
                        instaLoadLock = 0;
                        return true;
                    },
                    error: function(){
                        instaLoadLock = 0;
                        if(typeof options.onError === 'function'){
                            options.onError(self);
                        }
                        return false;
                    }
                });
            }
        },
        update : function()
        {
            if(instaLoadLock === 0){
                instaLoadLock = 1;
                $.ajax({
                    url: url + "&" + startType + "=" + nextMaxId,
                    dataType: "jsonp",
                    success: function (data) {
                        self.append(json2html.transform(data.data,item));
                        nextMaxId = data.pagination.next_max_id;
                        items = items.concat(data.data);

                        $('a > img',self).load(function(){
                            $(this).parent().addClass("visible");
                        });

                        instaLoadLock = 0;
                        return true;
                    },
                    error: function(){
                        instaLoadLock = 0;
                        return false;
                    }
                });
            }
        }
    };

    $.fn.streamInstagram = function(methodOrOptions){
        self = $(this);
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            return methods.init.apply( this, arguments );// Default to "init"
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.streamInstagram' );
        }
    };

})(jQuery);